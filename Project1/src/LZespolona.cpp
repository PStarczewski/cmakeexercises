#include <iostream>
#include <iomanip>
#include <cmath>

#include "LZespolona.hh"

using namespace std;

/*
 * Plik zawiera:
 * - definicje przeciazen operatorow arytmetycznych dzialajacych na strukturze LZespolona
 * - definicje przeciazen operatorow przesuniec bitowych realizujacych wyswietlanie i odbior lzespolonej poprzez operacje na strumieniach
 * - definicje funkcji wewnetrznej sprawdzajacej poprawnosc formatu wprowadzanej odpowiedzi
 */



/*
 * Podstawia do liczby zespolonej liczbę rzeczywistą.
 * Powoduje to, że w części urojonej zostanie automatycznie podstawiona
 * wartość 0.
 */
LZespolona &LZespolona::operator = (double  liczba)
{
  this->re = liczba; this->im = 0;
  return *this;
}

/*
 * Realizuje dodawanie dwoch liczb zespolonych.
 *    skl1 - pierwszy skladnik dodawania,
 *    skl2 - drugi skladnik dodawania.
 *    wynik - suma dwoch skladnikow przekazanych jako parametry.
 */
LZespolona  operator + (LZespolona  skl1,  LZespolona  skl2)
{
  LZespolona  wynik;

  wynik.re = skl1.re + skl2.re;
  wynik.im = skl1.im + skl2.im;
  return wynik;
}

/*
 * Realizuje odejmowanie dwoch liczb zespolonych.
 *    skl1 - pierwszy skladnik odejmowania,
 *    skl2 - drugi skladnik odejmowania.
 *    wynik - roznica dwoch skladnikow przekazanych jako parametry.
 */
LZespolona  operator - (LZespolona  skl1,  LZespolona  skl2)
{
  LZespolona  wynik;

  wynik.re = skl1.re - skl2.re;
  wynik.im = skl1.im - skl2.im;
  return wynik;
}

/*
 * Realizuje mnozenie dwoch liczb zespolonych.
 *    czyn1 - pierwszy skladnik mnozenia,
 *    czyn2 - drugi skladnik mnozenia.
 *    wynik - iloczyn dwoch skladnikow przekazanych jako parametry.
 */
LZespolona  operator * (LZespolona  czyn1,  LZespolona  czyn2)
{
  LZespolona  wynik;

  wynik.re = (czyn1.re * czyn2.re) - (czyn1.im * czyn2.im);
  wynik.im = (czyn1.im * czyn2.re) + (czyn2.im * czyn1.re);
  return wynik;
}

/*
 * Realizuje mnozenie liczby zespolonej i zmiennej double.
 *    czyn1 - pierwszy skladnik mnozenia,
 *    czyn2 - drugi skladnik mnozenia.
 *    wynik - iloczyn dwoch skladnikow przekazanych jako parametry.
 */
LZespolona  operator * (double  czyn1,  LZespolona  czyn2)
{
  LZespolona  wynik;

  wynik.re = czyn1 * czyn2.re;
  wynik.im = czyn1 * czyn2.im;
  return wynik;
}

/*
 * Realizuje mnozenie liczby zespolonej i zmiennej double.
 *    czyn1 - pierwszy skladnik mnozenia,
 *    czyn2 - drugi skladnik mnozenia.
 *    wynik - iloczyn dwoch skladnikow przekazanych jako parametry.
 */
LZespolona  operator * (LZespolona  czyn2, double  czyn1)
{
  LZespolona  wynik;

  wynik.re = czyn1 * czyn2.re;
  wynik.im = czyn1 * czyn2.im;
  return wynik;
}

/*
 * Realizuje dzielenie liczby zespolonej przez liczbe.
 *    dzielna - pierwszy skladnik dzielenia,
 *    dzielnik - drugi skladnik dzielenia.
 *    wynik - iloraz dwoch skladnikow przekazanych jako parametry.
 */
LZespolona operator / (LZespolona dzielna, double dzielnik)
{
  LZespolona wynik;

  wynik.re = dzielna.re/dzielnik;
  wynik.im = dzielna.im/dzielnik;

  return wynik;
}

/*
 * Realizuje dzielenie dwoch liczb zespolonych.
 *    dziel1 - dzielna skladnik dodawania,
 *    dziel2 - dzielnik skladnik dodawania.
 *    modul - oblicza kwadrat modulu dzielnika liczby zespolonej
 *    wynik - iloraz dwoch skladnikow przekazanych jako parametry.
 */
LZespolona  operator / (LZespolona  dziel1,  LZespolona  dziel2)
{
  LZespolona  wynik;
  double  modul;
  
  modul = (dziel2.re*dziel2.re) + (dziel2.im*dziel2.im);
  if(modul == 0) cerr << "Blad. Dzielenie przez zero." << endl;
  else{
    dziel2.im = -dziel2.im;
    return wynik = ((dziel1 * dziel2)/modul);
  }
  wynik = {0,0};
  return wynik;
}

bool  operator == (LZespolona zesp, double liczba)
{
  if(zesp.re == liczba && zesp.im == 0)
    return true;

  return false;
}

LZespolona operator - (LZespolona zesp)
{
  zesp.re = -zesp.re;
  zesp.im = -zesp.im;
  return zesp;
}

/*
 * Realizuje wyswietlenie liczby zespolonej poprzez referencje do strumienia wyjsciowego
 */
ostream& operator << (ostream &str_wyj, LZespolona l_zespolona)
{
     return str_wyj << '(' << l_zespolona.re << showpos << l_zespolona.im << 'i' << ')' << noshowpos ;
}

/*
 * Sprawdza poprawnosc znakow wpisanych jako odpowiedz, w przypadku bledu ustawia flage fail
 */
void WczytajZnak(istream& str_wej, char ten_znak)
 {
    char znak = ' ';
    
    str_wej >> znak;
    if(znak != ten_znak) 
      str_wej.setstate(ios::failbit);
 }

/*
 * Realizuje zapisanie liczby zespolonej poprzez referencje do strumienia wejsciowego
 */
istream& operator >> (istream &str_wej, LZespolona &l_zespolona)
{
     WczytajZnak(str_wej, '(');
     str_wej >> l_zespolona.re;
     str_wej >> l_zespolona.im;
     WczytajZnak(str_wej, 'i');
     WczytajZnak(str_wej, ')');
     return str_wej;
}