#include "TWektor.hh"
#include "rozmiar.h"
#include "LZespolona.hh"
#include "TMacierz.hh"
#include "TUklad.hh"

#include <iomanip>
#include <iostream>
#include <fstream>

using namespace std;

int main(int argc, char **argv)
{
  
  /*
   * Wyrażenie sprawdzające czy użytkownik wpisał nazwe pliku zawierajacego uklad
   */
  if (argc < 2) {
    cout << endl;
    cout << "Brak argumentu wywolania." << endl;
    cout << "Prosze wpisac nazwe pliku zawierajacym uklad rownan" << endl;
    cout << endl;
    return 1;
  }

  /* Otwarcie dostepu do pliku*/
  ifstream uklad;  
  uklad.open(argv[1]);


  TUklad<LZespolona,ROZMIAR> U1; 
  
  uklad >> U1;

  LZespolona x = {2,2};
  LZespolona y = {3,3};
  LZespolona z;
  z = y*x;
  cout << z;


  //Wyswietlenie macierzy wejsciowej
  cout << "Macierz wejsciowa" << endl;
  cout << setprecision(2) << U1.c_macierz << endl;
  
  //Transponowanie macierzy
  U1.c_macierz = U1.c_macierz.Transponuj();

  //Wyswietlenie macierzy transponowanej
  cout << "Macierz transponowana" << endl;
  cout << U1.c_macierz << endl;


  if(U1.MacierzTrojkatna()){
    if(U1.ObliczNiewiadome()){
      cout << U1;
    
      /*w = u.ObliczBlad();
      cout << setprecision(10) << "Wektor bledu:   Ax-b = ( " << w << ')' << endl;
      cout << setprecision(10) << "Dlugosc wektora bledu:  |Ax-b| = " << sqrt(w*w) << endl;*/
  }
  else{
    cerr << "BLAD. NASTAPILO DZIELENIE PRZEZ ZERO PRZY OBLICZANIU NIEWIADOMYCH. PROSZĘ RECZNIE PRZEMIENIC KOLUMNY." << endl;
    return 0;
  }
  }
  else{
    cerr << "BLAD. NASTAPILO DZIELENIE PRZEZ ZERO PRZY OBLICZANIU MACIERZY TROJKATNEJ. PROSZE RECZNIE PRZEMIENIC KOLUMNY." << endl;
    return 0;
  }

}
