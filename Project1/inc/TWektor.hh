#ifndef TWEKTOR_HH
#define TWEKTOR_HH

#include <iostream>

/*
 *  Szablon dla klasy modelujacej pojecie pojecie wektora macierzy.
 * 
 *  Zawiera:
 *  - deklaracje tablicy c_wektor przechowujacej dane
 *  - definicje konstruktora nadajacego wartosc poczatkowa
 *  - deklaracje przeciazen operatorow [][](dla wygody pisania)
 *  - deklaracje przeciazen operatorow arytmetycznych - + * / dla dzialan na wektorach
 */
template <typename typ, int wymiar>
class TWektor {

    private:

    typ c_wektor[wymiar];

    public:

    TWektor() {for(typ &i : c_wektor) i = 0;}

    typ operator [] (unsigned int index) const {return c_wektor[index];}
    typ &operator [] (unsigned int index)       {return c_wektor[index];}

    TWektor<typ,wymiar> operator - (const TWektor<typ,wymiar> &odjemnik) const;
    TWektor<typ,wymiar> operator + (const TWektor<typ,wymiar> &czynnik) const;
    TWektor<typ,wymiar> operator * (const typ &mnoznik) const;
    TWektor<typ,wymiar> operator / (const typ &dzielnik) const;
};

/*******************************METODY*******************************/

/*
 * Szablon definicji przeciazenia operatora -.
 * Metoda odejmuje odpowiednie elementy wektora 1 i wektora 2.
 */
template<typename typ, int wymiar>
TWektor<typ, wymiar> TWektor<typ, wymiar>::operator - (const TWektor<typ, wymiar> &odjemnik) const
{
    int i;
    TWektor<typ,wymiar>  wynik;

    for (i=0;i<wymiar;++i) 
        wynik[i] = (*this)[i] - odjemnik[i];
    
    return wynik;
}

/*
 * Szablon definicji przeciazenia operatora +.
 * Metoda dodaje odpowiednie elementy wektora 1 i wektora 2.
 */
template<typename typ, int wymiar>
TWektor<typ, wymiar> TWektor<typ, wymiar>::operator + (const TWektor<typ, wymiar> &czynnik) const
{
    int i;
    TWektor<typ,wymiar>  wynik;

    for (i=0;i<wymiar;++i) 
        wynik[i] = (*this)[i] + czynnik[i];
    
    return wynik;
}

/*
 * Szablon definicji przeciazenia operatora *.
 * Metoda mnozy odpowiednie elementy wektora przez stala typu zainicjowanego.
 */
template<typename typ, int wymiar>
TWektor<typ,wymiar> TWektor<typ,wymiar>::operator * (const typ &mnoznik) const
{
    int i;
    TWektor<typ,wymiar>  wynik;

    for (i=0;i<wymiar;++i) 
        wynik[i] = (*this)[i] * mnoznik;
    
    return wynik;
}

/*
 * Szablon definicji przeciazenia operatora /.
 * Metoda dzieli odpowiednie elementy wektora przez stala typu zainicjowanego.
 */
template<typename typ, int wymiar>
TWektor<typ,wymiar> TWektor<typ,wymiar>::operator / (const typ &dzielnik) const
{
    int i;
    TWektor<typ,wymiar>  wynik;

    for (i=0;i<wymiar;++i) 
        wynik[i] = (*this)[i] / dzielnik;
    
    return wynik;
}

/*******************************FUNKCJE*******************************/

/*
 * Szablon definicji przeciazenia operatora /.
 * Funkcja dzieli odpowiednie elementy wektora przez stala double.
 */
template<typename typ, int wymiar>
TWektor<typ,wymiar> operator / (const TWektor<typ,wymiar> dzielna, const double &dzielnik)
{
    int i;
    TWektor<typ,wymiar>  wynik;

    for (i=0;i<wymiar;++i) 
        wynik[i] = dzielna[i] / dzielnik;
    
    return wynik;
}

/*
 * Szablon definicji przeciazenia operatora *.
 * Funkcja mnozy odpowiednie elementy wektora przez stala double.
 */
template<typename typ, int wymiar>
TWektor<typ,wymiar> operator * (const TWektor<typ,wymiar> mnozna, const double &mnoznik)
{
    int i;
    TWektor<typ,wymiar>  wynik;

    for (i=0;i<wymiar;++i) 
        wynik[i] = mnozna[i] * mnoznik;
    
    return wynik;
}

/*
 * Szablon definicji przeciazenia operatora *.
 * Funkcja mnozy stala double przez odpowiednie elementy wektora.
 */
template<typename typ, int wymiar>
TWektor<typ,wymiar> operator * (const double &mnoznik, const TWektor<typ,wymiar> mnozna)
{
    int i;
    TWektor<typ,wymiar>  wynik;

    for (i=0;i<wymiar;++i) 
        wynik[i] = mnozna[i] * mnoznik;
    
    return wynik;
}

/*
 * Szablon definicji przeciazenia operatora strumienia wejscia.  
 * Umożliwia zapisanie danych do klasy Wektor.
 */
template<typename typ, int wymiar>
std::istream& operator >>  (std::istream &str_wej, TWektor<typ,wymiar> &wektor)
{
    int i;

    for(i=0;i<wymiar;++i){
        str_wej >> wektor[i];
    }
    return str_wej;
}

/*
 * Szablon definicji przeciazenia operatora strumienia wyjscia.  
 * Umożliwia wyswietlenie danych z klasy Wektor.
 */
template<typename typ, int wymiar>
std::ostream& operator <<  (std::ostream &str_wyj, const TWektor<typ,wymiar> &wektor)
{
    int i;

    for(i=0;i<wymiar;++i){
        str_wyj << " " << wektor[i];
    }
    return str_wyj;
}

#endif