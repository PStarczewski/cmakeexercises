#ifndef TMACIERZ_HH
#define TMACIERZ_HH

#include <iostream>

/*
 *  Szablon dla klasy modelujacej pojecie macierzy wyznacznikow.
 * 
 *  Zawiera:
 *  - deklaracje tablicy wektor przechowujacej macierz
 *  - deklaracje przeciazen operatorow [][](dla wygody pisania)
 *  - deklaracje przeciazenia operatora arytmetycznego * (macierz*wektor)
 *  - deklaracje metody transponujacej macierz A do postaci A^T
 */
template<typename typ,int wymiar>
class TMacierz {

    private:

    TWektor<typ,wymiar> c_macierz[wymiar];

    public:

    TWektor<typ,wymiar> operator [] (unsigned int index) const {return c_macierz[index];}
    TWektor<typ,wymiar> &operator [] (unsigned int index)       {return c_macierz[index];}

    TMacierz<typ,wymiar> Transponuj(void) const;
    TMacierz<typ,wymiar> operator * (const TWektor<typ,wymiar> &mnoznik) const;
    

};

/*******************************METODY*******************************/

/*
 * Szablon definicji metody transponujcej.
 * Metoda transponuje macierz z postaci A do A^T
 */
template<typename typ,int wymiar>
TMacierz<typ,wymiar> TMacierz<typ,wymiar>::Transponuj(void) const
{
    TMacierz<typ,wymiar> m_wyjsciowa;
    int i,j;

    for(i=0;i<wymiar;++i){
        for(j=0;j<wymiar;++j){
        m_wyjsciowa[i][j] = (*this)[j][i];
        }
    }
    return m_wyjsciowa;
}

/*
 * Szablon definicji przeciazenia operatora *.
 * Metoda mnozy odpowiednie elementy macierzy przez wektor.
 */
template<typename typ,int wymiar>
TMacierz<typ,wymiar> TMacierz<typ,wymiar>::operator * (const TWektor<typ,wymiar> &wektor) const
{
    TMacierz<typ,wymiar> m_wyjsciowa;
    int i,j;

    for(i=0;i<wymiar;++i){
        for(j=0;j<wymiar;++j){
        m_wyjsciowa[i][j] = (*this)[i][j] * wektor[j];
        }
    }
    return m_wyjsciowa;
}

/*******************************FUNKCJE*******************************/

/*
 * Szablon definicji przeciazenia operatora strumienia wejscia.  
 * Umożliwia zapisanie danych do klasy Macierz.
 */
template<typename typ, int wymiar>
std::istream& operator >>  (std::istream &str_wej, TMacierz<typ,wymiar> &macierz)
{
    int i;

    for(i=0;i<wymiar;++i){
        str_wej >> macierz[i];
    }
    return str_wej;
}

/*
 * Szablon definicji przeciazenia operatora strumienia wyjscia.  
 * Umożliwia wyswietlenie danych z klasy Macierz.
 */
template<typename typ, int wymiar>
std::ostream& operator <<  (std::ostream &str_wyj, const TMacierz<typ,wymiar> &macierz)
{
    int i;

    for(i=0;i<wymiar;++i){
        str_wyj << macierz[i] << std::endl;
    }
    return str_wyj;
}
#endif