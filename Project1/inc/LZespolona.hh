#ifndef LZESPOLONA_HH
#define LZESPOLONA_HH

#include <iostream>

/*
 *  Plik zawiera: 
 *  - definicje struktury LZespolona
 *  - deklaracje przeciazen operatorow arytmetycznych dzialajacych na tej strukturze.
 *  - deklaracje przeciazenia operatora przesuniecia bitowego realizujacego wyswietlanie poprzez operacje na strumieniach
 */



/*
 * Modeluje pojecie liczby zespolonej
 */
struct  LZespolona {
  double   re;    // Pole repezentuje czesc rzeczywista.
  double   im;    // Pole repezentuje czesc urojona.

  LZespolona &operator = (double  liczba); 
};

/*
 * Deklaracje przeciazen operatorow arytmetycznych
 */
LZespolona  operator + (LZespolona, LZespolona);
LZespolona  operator - (LZespolona, LZespolona);
LZespolona  operator * (LZespolona, LZespolona);
LZespolona  operator * (LZespolona, double);
LZespolona  operator * (double, LZespolona);
LZespolona  operator / (LZespolona, LZespolona);
LZespolona  operator / (LZespolona, double);
bool  operator == (LZespolona, double);
LZespolona operator - (LZespolona);
/*
 * Deklaracje przeciazen operatorow przesuniec bitowych
 */
std::ostream& operator << (std::ostream&, LZespolona);
std::istream& operator >> (std::istream&, LZespolona&);

#endif
