#ifndef TUKLADROWNANLINIOWYCH_HH
#define TUKLADROWNANLINIOWYCH_HH

#include <iostream>

/*
 *  Szablon dla klasy modelujacej pojecie ukladu rownan liniowych.
 * 
 *  Zawiera:
 *  - deklaracje zmiennych macierz i wektor przechowujacych macierz wspolczynnikow, 
 *  wyrazy wolne oraz wyrazy nieznane
 *  - deklaracje kopii macierzy wspolczynnikow oraz wektora wyrazow wolnego, na ktorych 
 *  przeprowadzane sa operacje arytmetyczne
 *  - deklaracje metody przeksztalcajacej macierz na macierz trojkatna dolna
 *  - deklaracje metody obliczajacej niewiadome xn.
 */
template<typename typ,int wymiar>
class TUklad {

    public:

    TMacierz<typ,wymiar> c_macierz;
    TWektor<typ,wymiar> c_wolne;
    TWektor<typ,wymiar> c_nieznane;

    TMacierz<typ,wymiar> c_macierz_kopia;
    TWektor<typ,wymiar> c_wolne_kopia;

    public:
    
    bool MacierzTrojkatna(void);
    bool ObliczNiewiadome(void);

};

/*******************************METODY*******************************/

/*
 *  Definicja funkcji obliczajacej niewiadome xn z macierzy trojkatnej.
 *  Funkcja zwraca true jesli wszystkie dzialania sie powioda, lub false jesli
 *  nastapilo dzielenie przez zero.
 */ 
template<typename typ,int wymiar>
bool TUklad<typ,wymiar>::ObliczNiewiadome()
{
    int i,j,k;
    typ czynniki;

    for(i=(wymiar-1);i>=0;--i){
        czynniki=0;
        k=(wymiar-1);
        
        for(j=i;j<(wymiar-1);++j){  
            czynniki = czynniki + (-(this->c_nieznane[k] * this->c_macierz_kopia[i][k]));
            --k;
        }    
        if(this->c_macierz_kopia[i][i] == 0) return false;

        this->c_nieznane[i] = (czynniki + this->c_wolne_kopia[i])/this->c_macierz_kopia[i][i];
    }
    return true;
}

/*
 *  Definicja funkcji przeksztalcajacej macierz na macierz trojkatna dolna.
 *  Funkcja zwraca true jesli wszystkie dzialania sie powioda, lub false jesli
 *  nastapilo dzielenie przez zero.
 */ 
template<typename typ,int wymiar>
bool TUklad<typ,wymiar>::MacierzTrojkatna(void)
{
    // Zmienne pomocnicze
    int i,j,k;
    typ m;

    this->c_macierz_kopia = this->c_macierz;
    this->c_wolne_kopia = this->c_wolne;

    for(i=0;i<(wymiar-1);++i){

        if(this->c_macierz_kopia[i][i] == 0) //Sprawdzenie czy dzielnik nie jest zerem
            return false;

        /*
         *  Dodanie do siebie wierszy w celu wyzerowania poszczegolnych elementow macierzy
         */ 
        for(j=(i+1);j<wymiar;++j){
            m = -(this->c_macierz_kopia[j][i]/this->c_macierz_kopia[i][i]);

            for(k=0;k<wymiar;++k){
                this->c_macierz_kopia[j][k] = this->c_macierz_kopia[j][k] + (m * this->c_macierz_kopia[i][k]);
            }

            /*
             *  Dodanie wyrazow wolnych tak samo jak elementow macierzy wspolczynnikow
             */
            this->c_wolne_kopia[j] = this->c_wolne_kopia[j] + (m * this->c_wolne_kopia[i]);
        } 
    }
    return true;
}

/*******************************FUNKCJE*******************************/

/*
 * Szablon definicji przeciazenia operatora strumienia wejscia.  
 * Umożliwia zapisanie danych do klasy Uklad.
 */
template<typename typ, int wymiar>
std::istream& operator >> (std::istream &str_wej, TUklad<typ,wymiar> &uklad)
{
    str_wej >> uklad.c_macierz;
    str_wej >> uklad.c_wolne;

    return str_wej;
}

/*
 * Szablon definicji przeciazenia operatora strumienia wyjscia.  
 * Umożliwia wyswietlenie danych z klasy Uklad.
 */
template<typename typ, int wymiar>
std::ostream& operator <<  (std::ostream &str_wyj, const TUklad<typ,wymiar> &uklad)
{

        str_wyj << "Macierz trojkatna:\n" <<  uklad.c_macierz_kopia << "\n";
        str_wyj << "Wektor wyrazow wolnych:\n" << uklad.c_wolne << "\n"  << std::endl;
        str_wyj << "Szukane x1, x2, x3:\n" << uklad.c_nieznane << "\n" << std::endl;

    return str_wyj;
}

#endif