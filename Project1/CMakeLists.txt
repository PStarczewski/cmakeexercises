cmake_minimum_required(VERSION 2.6)

project(Project1)

file(GLOB_RECURSE Project1_SOURCES "src/*.cpp")
file(GLOB_RECURSE Project1_HEADERS "inc/*.h")


set (Project1_INCLUDE_DIRS "")
foreach (_headerFile ${Project1_HEADERS})
    get_filename_component(_dir ${_headerFile} PATH)
    list (APPEND Project1_INCLUDE_DIRS ${_dir})
endforeach()
list(REMOVE_DUPLICATES Project1_INCLUDE_DIRS)


include_directories("${PROJECT_SOURCE_DIR}/src")
add_subdirectory(src)
set (EXTRA_LIBS ${EXTRA_LIBS} inc)


add_executable (Project1Exe ${Project1_SOURCES})
target_include_directories(Project1Exe PRIVATE ${Project1_INCLUDE_DIRS})


